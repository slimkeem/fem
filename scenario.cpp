#include "scenario.h"

#include "femobject.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <parametrics/visualizers/gmpsurfnormalsvisualizer.h>

#include <trianglesystem/gmtrianglesystem.h>
#include <parametrics/curves/gmpcircle.h>

// qt
#include <QQuickItem>



void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8, 0.002, 0.0008);
  scene()->insertLight( light, false );

  // Insert Sun
  scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3> init_cam_pos(  0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  0.0f, 0.0f, 1.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Front cam
  auto front_rcpair = createRCPair("Front");
  front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -50.0f, 0.0f ), init_cam_dir, init_cam_up );
  front_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( front_rcpair.camera.get() );
  front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Side cam
  auto side_rcpair = createRCPair("Side");
  side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -50.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
  side_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( side_rcpair.camera.get() );
  side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Top cam
  auto top_rcpair = createRCPair("Top");
  top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 50.0f ), -init_cam_up, init_cam_dir );
  top_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( top_rcpair.camera.get() );
  top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );



  // Init Regular Triangulation
  initializeElasticMembrane();

  //  Init Random Trinagulation
  initializeRandomElasticMembrane();

}

void Scenario::cleanupScenario() {

  if (m_elastic_membrane != nullptr)
    cleanupElasticMembrane();
  if (m_random_elastic_membrane != nullptr)
    cleanupRandomElasticMembrane();
}

void Scenario::initializeElasticMembrane() {

  m_elastic_membrane = std::make_shared<GMlib::FEMObject>();

  m_elastic_membrane->regularTriangulation(5,5,5);
  m_elastic_membrane->Computation();
  m_elastic_membrane->UpdateHeight(1);
  m_elastic_membrane->setMaxForce(2);
  m_elastic_membrane->toggleDefaultVisualizer();
  m_elastic_membrane->setMaterial(GMlib::GMmaterial::snow());
  m_elastic_membrane->replot();

  scene()->insert(m_elastic_membrane.get());
}

void Scenario::cleanupElasticMembrane()
{
  scene()->remove(m_elastic_membrane.get());
  m_elastic_membrane.reset();
}

void Scenario::initializeRandomElasticMembrane()
{

  m_random_elastic_membrane = std::make_shared<GMlib::FEMObject>();

  m_random_elastic_membrane->randomTriangulation(40,5);
  m_random_elastic_membrane->Computation();
  m_random_elastic_membrane->UpdateHeight(1);
  m_random_elastic_membrane->setMaxForce(2);
  m_random_elastic_membrane->toggleDefaultVisualizer();
  m_random_elastic_membrane->setMaterial(GMlib::GMmaterial::blackPlastic());
  m_random_elastic_membrane->translateGlobal(GMlib::Vector<float,3>(15.0,4.0,0.0));
  m_random_elastic_membrane->replot();

  scene()->insert(m_random_elastic_membrane.get());
}

void Scenario::cleanupRandomElasticMembrane()
{
  scene()->remove(m_random_elastic_membrane.get());
  m_random_elastic_membrane.reset();
}

void Scenario::animateMembrane()
{
  m_elastic_membrane->replot();
  m_random_elastic_membrane->replot();
}
