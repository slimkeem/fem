#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

namespace GMlib {
  template <typename T> class TriangleFacets;
  class FEMObject;
}

class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;
  void    animateMembrane();


private:

  void initializeElasticMembrane();
  void cleanupElasticMembrane();

  void initializeRandomElasticMembrane();
  void cleanupRandomElasticMembrane();

  std::shared_ptr<GMlib::FEMObject> m_elastic_membrane{nullptr};
  std::shared_ptr<GMlib::FEMObject> m_random_elastic_membrane{nullptr};

};

#endif // SCENARIO_H
