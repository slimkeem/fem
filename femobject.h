#ifndef FEMOBJECT_H
#define FEMOBJECT_H

// gmlib
#include <trianglesystem/gmtrianglesystem.h>
#include <core/containers/gmarray.h>
#include "node.h"

namespace GMlib {
  class FEMObject : public TriangleFacets<float>
  {
  private:
    ArrayLX<Node> _nodes; //an array of nodes
    DMatrix<float> _A;    //stiffness matrix
    DVector<float> _b;    //load vector
    float _f;             //force
    float max_f;
    bool _atTop;
    bool _atBottom;

  public:
    FEMObject(){
      _f = 0;
      _atTop = true;
      _atBottom = false;
    };
    ~FEMObject() override{};

    void regularTriangulation(int n, int m, float r);
    void randomTriangulation(int n, float r);
    void Computation();
    Vector<Vector<float, 2>, 3> findNeighborEdges(TSEdge<float> *edge);
    Vector<Vector<float, 2>, 3> findNodeEdges(Node *n, TSTriangle<float> *triangle);
    void setMaxForce(float f);
    float getMaxForce() const;
    void UpdateHeight(float f);

    void localSimulate( double dt) override {
      if(_atTop) {
        _f += dt;
        if( _f > max_f) {
          _atTop = false;
          _atBottom = true;
        }
      }
      if(_atBottom){
        _f -= dt;
        if( _f < max_f) {
          _atTop = true;
          _atBottom = false;
        }
      }

      // updating the height
      UpdateHeight(_f);
    }

  };
}

#endif // FEMOBJECT_H
