#include "femobject.h"
#include <cmath>

#include <time.h>

namespace GMlib {

void GMlib::FEMObject::regularTriangulation(int n, int m, float r)
{
  this->insertAlways(TSVertex<float>(Point<float,2> (0.0, 0.0))); //add points to the mesh
    for (auto i=1; i<m; i++){
      for (auto j=0; j<n; j++){
        Angle alpha = j*M_2PI/n; //rotation angle
        SqMatrix<float,2>R(alpha,Vector<float,2>(1,0),Vector<float,2>(0,1));
        Point<float, 2> p = R * Point<float, 2>(i*r/m, 0);

        this->insertAlways(TSVertex<float>(p)); //add points to the mesh

      }
      n*=2;
    }
//    this->triangulateDelaunay(); //creates mesh
  }


  void GMlib::FEMObject::randomTriangulation(int n, float r)
  {

    auto rn = 4;
    int m = n / rn;

    FEMObject::regularTriangulation(rn, m, r);

    float nm1 = ((M_PI / (std::sqrt(3) * std::pow(std::sin(M_PI / n) ,2)) + (2 - n)) / (0.5*n));
    float nm2 = 1.1;
    float nm = std::max(nm1, nm2);

    int num = 1 + (n * nm);
    srand (time(NULL));

    for( auto i = 0; i < this->size(); i++ ) {
      std::swap((*this)[rand() % this->size()], (*this)[rand() % this->size()]);
    }

    int size = this->size();

    for( int i = num; i <= size; i++ ) {

      auto parameters = this->getVertex(i)->getParameter();

      auto check = std::sqrt( (parameters[0] * parameters[0]) + (parameters[1] * parameters[1]) );

      // Node is not a boundary point
      if( ( std::abs(check) != r) )
        this->removeIndex(i);
    }
//    this->triangulateDelaunay(); //creates mesh
  }


  void GMlib::FEMObject::Computation()
  {
    this->triangulateDelaunay();
    //Fill in an array of nodes
    for (auto i = 0; i < this->getNoVertices(); i++){
      if (!this->getVertex(i)->boundary()) //check if its  boundary
        this->_nodes += Node(this->getVertex(i));
    }

    _A.setDim(_nodes.size(),_nodes.size());
    _b.setDim(_nodes.size());
    //_f = -1;

    //Assemble the stiffness matrix:
    for (int i=0; i<_nodes.size(); i++){
      for (int j=0; j<i; j++){
        TSEdge<float>* edge = _nodes[i].getNeighbor(_nodes[j]);
        if (edge){
          Vector<Vector<float, 2>, 3> V = findNeighborEdges(edge);
          auto d = V[0];
          auto a1 = V[1];
          auto a2 = V[2];

          auto dd = 1/std::abs(d*d);
          auto dh1 = dd * (a1 * d);
          auto dh2 = dd * (a2 * d);
          auto area1 = std::abs(d^a1);
          auto area2 = std::abs(d^a2);
          auto h1 = dd * area1*area1;
          auto h2 = dd * area2*area2;

          //compute non-diagonal element of the stiffness matrix
          _A[i][j] = _A[j][i] = (dh1 * (1 - dh1)/h1 - dd) * area1/2 +
              (dh2 * (1 - dh2)/h2 - dd) * area2/2;
        }
        else {_A[i][j] = _A[j][i] = 0;}
      }


      Array<TSTriangle<float>* > triangles = _nodes[i].getTriangles();
      float sum = 0.0f;
      float area_sum = 0.0f;
      for (auto j=0; j<triangles.size(); j++){
        auto V = findNodeEdges(&_nodes[i],triangles[j]);
        auto d1 = V[0];
        auto d2 = V[1];
        auto d3 = V[2];

        sum+= d3*d3/(2*std::abs(d1^d2));

        area_sum += triangles[j]->getArea();

      }
      //compute diagonal element of the stiffness matrix
      _A[i][i] = sum;

      //Compute the load vector in the similar way
      _b[i]=/*_f * */ 1/3 * area_sum * 1;

    }

  }



  void FEMObject::UpdateHeight(float f)
  {
    //Solve the matrix
    DVector<float> x = _A.invert() * f *_b;

    //Set solution to internal vertices
    for (auto i=0; i<_nodes.size(); i++)
      _nodes[i].Node::setZ(x[i]);
    }

  Vector<Vector<float, 2>, 3> FEMObject::findNeighborEdges(TSEdge<float> *edge)
  {
    TSVertex<float>* p1 = edge->getFirstVertex();
    TSVertex<float>* p2 = edge->getLastVertex();
    Array<TSTriangle<float>*> T = edge->getTriangle();

    TSVertex<float>* p3 = nullptr;
    TSVertex<float>* p4 = nullptr;

    Vector<Vector<float, 2>, 3> V;// = nullptr;

    Array<TSVertex<float>*> V1 = T[0]->getVertices();
    for (auto i=0; i<3; i++){
      if (V1[i] != p1 and V1[i] != p2)
        p3 = V1[i];
    }
    Array<TSVertex<float>*> V2 = T[1]->getVertices();
    for (auto i=0; i<3; i++){
      if (V2[i] != p1 and V2[i] != p2)
        p4 = V2[i];
    }

    Vector<float,2> p1_coord = p1->getParameter();
    Vector<float,2> p2_coord = p2->getParameter();
    Vector<float,2> p3_coord = p3->getParameter();
    Vector<float,2> p4_coord = p4->getParameter();

    V[0]= p2_coord-p1_coord;
    V[1]= p3_coord-p1_coord;
    V[2]= p4_coord-p1_coord;

    return V;
  }

  Vector<Vector<float, 2>, 3> FEMObject::findNodeEdges(Node *n, TSTriangle<float> *triangle)
  {

    auto tv = triangle->getVertices();
    TSVertex<float> *p0;
    TSVertex<float> *p1;
    TSVertex<float> *p2;

    Vector<Vector<float, 2>, 3> V;// = nullptr;

    if (tv[0]==n->getVertex()){
      p0 = tv[0];
      p1 = tv[1];
      p2 = tv[2];
    }
    else if (tv[1]==n->getVertex()) {
      p0 = tv[1];
      p1 = tv[0];
      p2 = tv[2];
    }
    else {
      p0 = tv[2];
      p1 = tv[0];
      p2 = tv[1];
    }
    V[0]= p1->getParameter()-p0->getParameter();
    V[1]= p2->getParameter()-p0->getParameter();
    V[2]= p2->getParameter()-p1->getParameter();

    return V;
  }

  void FEMObject::setMaxForce(float f)
  {
    max_f = f;
  }

}// End of Namespace
