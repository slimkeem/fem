#include "node.h"

namespace GMlib {

GMlib::Array<GMlib::TSTriangle<float> *> GMlib::Node::getTriangles()
{
  return _v->getTriangles();
}

GMlib::TSEdge<float> *GMlib::Node::getNeighbor(GMlib::Node n)
{
  ArrayT<TSEdge<float>*> edges = _v->getEdges();
  for(int i = 0; i < edges.size(); ++i){
    if((edges[i]->getOtherVertex(*_v)) == (n._v))
      return edges[i];
  }
  return nullptr;
}

void GMlib::Node::setZ(float z)
{
  _v->setZ(z);
}

TSVertex<float> *GMlib::Node::getVertex()
{
  return _v;
}

GMlib::Node::Node(TSVertex<float>* v)
{
  _v = v;
}

GMlib::Node::Node(){}

}
