#ifndef NODE_H
#define NODE_H

// gmlib
#include <trianglesystem/gmtrianglesystem.h>

namespace GMlib {
class Node
{
private:
  TSVertex<float> *_v; //the node vertex

public:
  Array<GMlib::TSTriangle<float> *> getTriangles();
  TSEdge<float> *getNeighbor(Node n); //return an edge if n is the neighbor node and NULL otherwise
  void setZ(float z); //specify z-coordinate of the vertex
  TSVertex<float>* getVertex();
  Node(TSVertex<float>* v);
  Node();
};
}

#endif // NODE_H
